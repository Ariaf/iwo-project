# My project's README

I want to study the use of emoticons/emojis in Dutch twitter data. I want to find out
if there is a significant difference in the use of emoticons/emojis regarding gender.
H: There is a difference sentiment expressed through emoticons with respect to gender.
H: Female Twitter users expresses their sentiment more often through smiley's than the male users.

First I will pre-test which use of emoticons is more common in the Twitter data (i.e.
emoticons like; :), :D, :(, etc. or emojis). And I will distinguish gender by the given script provided by B. Plank.

The variable gender (IV) has influence on the use of emoticons/emojis (DV).
�	Independent variable: Gender
�	Dependent variable: Use of emoticons/emojis

I will use in-house Twitter data.

# Feedback

Goed onderscheid gemaakt tussen de OV en de AV. Goed dat je ook al hebt nagedacht over H0 en H1. Formuleer het wel wat specifieker dan (er is een verschil / er is geen verschil) waar is geen verschil tussen? etc. 
Je hebt nu de hypothese als tweezijdig geformuleerd. Voor meer info: http://wetenschap.infonu.nl/diversen/110297-verschillen-tussen-twee-datasets-de-t-test.html
Aangezien je in dit onderzoek gewoon de aantallen kunt tellen zou ik het gewoon eenzijdig formuleren. Formulier ook iets preciezer. bijvoorbeeld: vrouwen gebruiken vaker :D dan mannen. 
Maak het jezelf niet te moeilijk en kijk bijvoorbeeld gewoon naar een paar emoties. Denk bijvoorbeeld aan blije emoties tegenover verdrietige emoties. Ga ook nadenken over hoe je de steekproef gaat trekken. Kijk of je genoeg emoties kan vinden als je 1 dag pakt bijvoorbeeld. 