import sys

female=[name.strip() for name in open("names/nl-female.txt").readlines()]
male=[name.strip() for name in open("names/nl-male.txt").readlines()]

for line in sys.stdin:
    name = line.strip().split("\t")[-1].lower() # assumes name is last in tab-separated input
    f = m = 0
    for n in female:
        if n in name:
            f+=1
    for n in male:
        if n in name:
            m+=1

    if f > m:
        print("{}\tfemale".format(line.strip()))
    elif m > f:
        print("{}\tmale".format(line.strip()))
    else:
        pass #not found
