#!/bin/bash

# Unique tweets that are no retweets of the 8th of march with ':)' or ':-)' placed in a txt file for further use
echo "All the unique tweets (RT excluded) of 8th of march that contain a positive smiley placed in a txt file"
zless /net/corpora/twitter2/Tweets/2017/03/20170308*.out.gz  | /net/corpora/twitter2/tools/tweet2tab -i text user | awk '!x[$0]++' | grep -v ^RT | grep -v ^RT | grep '\:)\|\:-)' > happyfacedata.txt

# Unique tweets that are no retweets of the 8th of march with ':(' or ':-(' placed in a txt file for further use
echo "All the unique tweets (RT excluded) of 8th of march that contain a negative smiley placed in a txt file"
zless /net/corpora/twitter2/Tweets/2017/03/20170308*.out.gz  | /net/corpora/twitter2/tools/tweet2tab -i text user | awk '!x[$0]++' | grep -v ^RT | grep -v ^RT | grep '\:(\|\:-(' > sadfacedata.txt

# Unique tweets that are no retweets of the 8th of march
echo "All the unique tweets (RT excluded) of 8th of march"
zless /net/corpora/twitter2/Tweets/2017/03/20170308*.out.gz  | /net/corpora/twitter2/tools/tweet2tab -i text user | awk '!x[$0]++' | grep -v ^RT | grep -v ^RT  > alldata.txt

# To figure out the gender of the twitteruser we run the get-gender with the txt files we made beforehand and save the lines with their respective gender in a new txt file
echo "All the collected ':)/:-)' tweets with their twitteruser gender at the end of the line"
cat happyfacedata.txt | python3 get-gender.py > happyfacedataGender.txt
echo "All the collected ':(/:-(' tweets with their twitteruser gender at the end of the line"
cat sadfacedata.txt | python3 get-gender.py > sadfacedataGender.txt
echo "All data collected with their twitteruser gender at the end of the line"
cat alldata.txt | python3 get-gender.py > alldataGender.txt

# Sentiment analysis:
# Amount of tweets per gender with respect to either positive ':)/:-)' or negative ':(/:-(' tweets
echo "Amount of tweets per gender with respect to either positive ':)/:-)' or negative ':(/:-(' tweets"
cat happyfacedataGender.txt | grep -w 'male' | wc -l
cat happyfacedataGender.txt | grep -w 'female' | wc -l
cat sadfacedataGender.txt | grep -w 'male' | wc -l
cat sadfacedataGender.txt | grep -w 'female' | wc -l
cat alldataGender.txt | grep -w 'male' | wc -l
cat alldataGender.txt | grep -w 'female' | wc -l



